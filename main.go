package main

import (
	"fmt"

	"gitlab.com/gyanchand/cncf/app"
)

func main() {
	fmt.Println("main go start:::")
	app.StartApplication()
}
