package api

import (
	"encoding/json"
	"sync"

	"github.com/prometheus/prometheus/prompb"
	logger "github.com/rs/zerolog/log"
	"gitlab.com/gyanchand/cncf/kafka"
)

//DataToSend :
type DataToSend struct {
	Metric struct {
		Instance string `json:"instance"`
		Job      string `json:"job"`
		Name     string `json:"__name__"`
	} `json:"metric"`
	Values [][]interface{} `json:"values"`
}

//Labelmap :
var Labelmap map[string]string

//LabelMapping :
func labelMapping(s *ServiceHandler, req prompb.WriteRequest) {
	var nwData, infraData, applicationData, containerData []DataToSend
	templabelmap := make(map[string]DataToSend)
	for _, sliceofTimeseries := range req.GetTimeseries() {
		var data DataToSend
		for _, label := range sliceofTimeseries.Labels {
			if label.GetName() == "__name__" {
				data.Metric.Name = label.GetValue()
				continue
			}
			if label.GetName() == "instance" {
				data.Metric.Instance = label.GetValue()
				continue
			}
			if label.GetName() == "job" {
				data.Metric.Job = label.GetValue()
			}
		}
		data.Values = getValues(sliceofTimeseries.Samples)
		key := data.Metric.Name + data.Metric.Instance
		v, ok := templabelmap[key]
		if !ok {
			templabelmap[key] = data
		} else {
			v.Values = append(v.Values, data.Values...)
			templabelmap[key] = v
		}
		// if templabelmap[key].Metric.Name == "" {
		// 	templabelmap[key] = data
		// } else {
		// 	temp := templabelmap[key]
		// 	temp.Values = append(temp.Values, data.Values...)
		// }
	}
	for _, v := range templabelmap {
		topic := find(v.Metric.Name)
		switch topic {
		case "network":
			nwData = append(nwData, v)
		case "infra":
			infraData = append(infraData, v)
		case "application":
			applicationData = append(applicationData, v)
		case "container":
			containerData = append(containerData, v)
		}
	}
	var wg sync.WaitGroup
	wg.Add(4)
	go sendtoKafka(&wg, s.ProducerWriter, "network", nwData)
	go sendtoKafka(&wg, s.ProducerWriter, "infra", infraData)
	go sendtoKafka(&wg, s.ProducerWriter, "application", applicationData)
	go sendtoKafka(&wg, s.ProducerWriter, "container", containerData)
	wg.Wait()
}

//Find :
func find(val string) string {
	return Labelmap[val]
}

//GetValues :
func getValues(rcv []prompb.Sample) [][]interface{} {
	var val [][]interface{}
	for _, v := range rcv {
		var temp []interface{}
		temp = append(temp, v.Value, v.Timestamp)
		val = append(val, temp)
	}
	return val
}

//SendtoKafka :
func sendtoKafka(wg *sync.WaitGroup, s kafka.WriteMsg, topic string, data []DataToSend) {
	if len(data) < 1 {
		wg.Done()
		return
	}
	bytevalue, _ := json.Marshal(data)
	p, o, err := s.SendMessage(kafka.NewJSONMessage(topic, bytevalue))
	if err != nil {
		logger.Error().Err(err).Msg("error push")
		wg.Done()
		return
	}
	logger.Info().Msgf("Topic: %s, Partition: %d, Offset: %d", topic, p, o)
	wg.Done()
	return
}
