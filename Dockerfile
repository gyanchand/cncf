FROM golang:alpine

ENV SOURCES /go/src/gitlab.com/gyanchand/cncf/

COPY . ${SOURCES}

RUN cd ${SOURCES} && CGO_ENABLED=0 go install

ENV PORT 8080
EXPOSE 8080

ENTRYPOINT [ "cncf" ]