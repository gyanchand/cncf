package app

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/Shopify/sarama"
	"github.com/gin-gonic/gin"
	logger "github.com/rs/zerolog/log"
	"gitlab.com/gyanchand/cncf/api"
	"gitlab.com/gyanchand/cncf/kafka"
)

var (
	router                                 *gin.Engine
	address, kafkaBrokerURL, kafkaClientID string
	srvhandler                             api.ServiceHandler
)

func init() {
	gin.SetMode(gin.ReleaseMode)
	router = gin.Default()
	flag.StringVar(&address, "port", "8080", "port to connect app")
	flag.StringVar(&kafkaBrokerURL, "kafka-brokers", "localhost:29092", "Kafka brokers in comma separated value")
	flag.StringVar(&kafkaClientID, "kafka-client-id", "my-kafka-client", "Kafka client id to connect")
	flag.Parse()
	broker := sarama.NewBroker(kafkaBrokerURL)
	err := broker.Open(nil)
	if err != nil {
		logger.Fatal().Err(err).Msg("Open tries to connect to broker failed:::")
	}

}

//StartApplication :
func StartApplication() {
	producer, err := kafka.InitProducer(kafkaClientID, kafkaBrokerURL)
	if err != nil {
		logger.Fatal().Err(err).Msg("Failed to init producer")
	}
	srvhandler.ProducerWriter = producer
	mapurl()
	srv := &http.Server{
		Addr:    ":" + address,
		Handler: router,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logger.Fatal().Err(err).Msg("Server Shutdown: ")
	}
	select {
	case <-ctx.Done():
		log.Println("timeout of 2 seconds.")
	}
	log.Println("Server Shutdown Gracefully")
}
