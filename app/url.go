package app

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func mapurl() {
	routerGroup := router.Group("/api")
	routerGroup.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"Message": "OK",
		})
		log.Println("Health is OK")
	})
	routerGroup.POST("/receive", srvhandler.ReceiveAPI)

}
