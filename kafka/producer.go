package kafka

import (
	"log"
	"os"

	"github.com/Shopify/sarama"
)

//WriteMsg :
type WriteMsg interface {
	SendMessage(msg *sarama.ProducerMessage) (partition int32, offset int64, err error)
}

//InitProducer :
func InitProducer(kafkaClientID, kafkaBrokerURL string) (sarama.SyncProducer, error) {
	// setup sarama log to stdout
	sarama.Logger = log.New(os.Stdout, "", log.Ltime)

	// producer config
	config := sarama.NewConfig()
	config.ClientID = kafkaClientID
	// config.Version = sarama.V2_0_0_0
	config.Producer.Retry.Max = 2
	config.Producer.Partitioner = sarama.NewRandomPartitioner
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Return.Successes = true

	// async producer
	//prd, err := sarama.NewAsyncProducer([]string{kafkaConn}, config)

	// sync producer
	producer, err := sarama.NewSyncProducer([]string{kafkaBrokerURL}, config)
	if err != nil {
		return nil, err
	}
	log.Println("Producer Initiation Done...")
	return producer, nil
}

// NewMessage :
func NewMessage(topic, message string) *sarama.ProducerMessage {
	return &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(message),
	}
}

// NewJSONMessage :
func NewJSONMessage(topic string, message []byte) *sarama.ProducerMessage {
	return &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.ByteEncoder(message),
	}
}
