module gitlab.com/gyanchand/cncf

go 1.13

require (
	github.com/Shopify/sarama v1.19.0
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.1
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db
	github.com/micro/go-micro v1.18.0 // indirect
	github.com/prometheus/prometheus v2.5.0+incompatible
	github.com/rs/zerolog v1.20.0
	google.golang.org/protobuf v1.25.0
)
